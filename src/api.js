import axios from 'axios';

const API_URL = 'http://localhost:9000';

export const addUser = (id, username) => axios.post(`${API_URL}/users?username=${username}`, { id });
export const saveMaterialpart = (materialpart) => axios.post(`${API_URL}/mpl`, materialpart);
