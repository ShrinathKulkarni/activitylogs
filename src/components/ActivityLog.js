import React, { useState } from 'react';
import { saveMaterialpart } from '../api';

const ActivityLog = () => {
  const [activity, setActivity] = useState({
    userid: '',
    date: '',
    activitydescription: '',
    hoursspent: '',
    location: '',
    performedby: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setActivity((prevActivity) => ({ ...prevActivity, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await saveMaterialpart(activity);
      alert('Activity log saved successfully');
    } catch (error) {
      console.error('Error saving activity log:', error);
    }
  };

  return (
    <div>
      <h1>Activity Log</h1>
      <form onSubmit={handleSubmit}>
        <label>
          User ID:
          <input type="text" name="userid" value={activity.userid} onChange={handleChange} required />
        </label>
        <label>
          Date:
          <input type="datetime-local" name="date" value={activity.date} onChange={handleChange} required />
        </label>
        <label>
          Activity Description:
          <input type="text" name="activitydescription" value={activity.activitydescription} onChange={handleChange} required />
        </label>
        <label>
          Hours Spent:
          <input type="number" name="hoursspent" value={activity.hoursspent} onChange={handleChange} required />
        </label>
        <label>
          Location:
          <input type="text" name="location" value={activity.location} onChange={handleChange} required />
        </label>
        <label>
          Performed By:
          <input type="text" name="performedby" value={activity.performedby} onChange={handleChange} required />
        </label>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default ActivityLog;
